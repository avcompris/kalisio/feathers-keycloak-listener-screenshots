

_[Back to the home page](../../README.md)_




# Project: feathers-keycloak-listener



## Test: keycloak_setUp.js


### Description




 
> sets up Keycloak for integration tests with the kApp

### Steps (40) — 55 sec
1. [Keycloak: Login page](00000001.md)
2. [Credentials](00000002.md)
3. [Submit the login form](00000003.md)
4. [Deploy the realm list](00000004.md)
5. [Add a realm](00000005.md)
6. [Fill in the realm form](00000006.md)
7. [Submit the realm form](00000007.md)
8. [Go to the realm settings](00000008.md)
9. [Open the Events tab](00000009.md)
10. [Open the popup listbox](00000010.md)
11. [Select: "keycloak-event-gateway" in the listbox](00000011.md)
12. [Toggle the popup listbox](00000012.md)
13. [Actually save the config](00000013.md)
14. [Go to the users page](00000014.md)
15. [Ask to create a user](00000015.md)
16. [Fill in the user form](00000016.md)
17. [Submit the user form](00000017.md)
18. [Go to the Attributes tab](00000018.md)
19. [Ask to add a new attribute](00000019.md)
20. [Fill in attribute values](00000020.md)
21. [Save the attributes](00000021.md)
22. [Go to the users page](00000022.md)
23. [Ask to create a user](00000023.md)
24. [Fill in the user form](00000024.md)
25. [Submit the user form](00000025.md)
26. [Open the Credentials panel](00000026.md)
27. [Ask to set the password](00000027.md)
28. [Fill in the password form](00000028.md)
29. [Submit the password form](00000029.md)
30. [Confirm](00000030.md)
31. [Go to the clients page](00000031.md)
32. [Ask to create a client](00000032.md)
33. [Fill in the general settings](00000033.md)
34. [Go to the next page](00000034.md)
35. [Add client authentication](00000035.md)
36. [Go to the next page, again](00000036.md)
37. [Fill in the client settings](00000037.md)
38. [Submit the client form](00000038.md)
39. [Go to the Credentials tab](00000039.md)
40. [Get the client's secret](00000040.md)


### Screenshots


|  **1. Keycloak: Login page** | **2. Credentials** | **3. Submit the login form** | **4. Deploy the realm list** |
| :--: | :--: | :--: | :--: |
|  [![00000001.png](./00000001.png)](00000001.md) | [![00000002.png](./00000002.png)](00000002.md) | [![00000003.png](./00000003.png)](00000003.md) | [![00000004.png](./00000004.png)](00000004.md) |
|  **5. Add a realm** | **6. Fill in the realm form** | **7. Submit the realm form** | **8. Go to the realm settings** |
|  [![00000005.png](./00000005.png)](00000005.md) | [![00000006.png](./00000006.png)](00000006.md) | [![00000007.png](./00000007.png)](00000007.md) | [![00000008.png](./00000008.png)](00000008.md) |
|  **9. Open the Events tab** | **10. Open the popup listbox** | **11. Select: "keycloak-event-gateway" in the listbox** | **12. Toggle the popup listbox** |
|  [![00000009.png](./00000009.png)](00000009.md) | [![00000010.png](./00000010.png)](00000010.md) | [![00000011.png](./00000011.png)](00000011.md) | [![00000012.png](./00000012.png)](00000012.md) |
|  **13. Actually save the config** | **14. Go to the users page** | **15. Ask to create a user** | **16. Fill in the user form** |
|  [![00000013.png](./00000013.png)](00000013.md) | [![00000014.png](./00000014.png)](00000014.md) | [![00000015.png](./00000015.png)](00000015.md) | [![00000016.png](./00000016.png)](00000016.md) |
|  **17. Submit the user form** | **18. Go to the Attributes tab** | **19. Ask to add a new attribute** | **20. Fill in attribute values** |
|  [![00000017.png](./00000017.png)](00000017.md) | [![00000018.png](./00000018.png)](00000018.md) | [![00000019.png](./00000019.png)](00000019.md) | [![00000020.png](./00000020.png)](00000020.md) |
|  **21. Save the attributes** | **22. Go to the users page** | **23. Ask to create a user** | **24. Fill in the user form** |
|  [![00000021.png](./00000021.png)](00000021.md) | [![00000022.png](./00000022.png)](00000022.md) | [![00000023.png](./00000023.png)](00000023.md) | [![00000024.png](./00000024.png)](00000024.md) |
|  **25. Submit the user form** | **26. Open the Credentials panel** | **27. Ask to set the password** | **28. Fill in the password form** |
|  [![00000025.png](./00000025.png)](00000025.md) | [![00000026.png](./00000026.png)](00000026.md) | [![00000027.png](./00000027.png)](00000027.md) | [![00000028.png](./00000028.png)](00000028.md) |
|  **29. Submit the password form** | **30. Confirm** | **31. Go to the clients page** | **32. Ask to create a client** |
|  [![00000029.png](./00000029.png)](00000029.md) | [![00000030.png](./00000030.png)](00000030.md) | [![00000031.png](./00000031.png)](00000031.md) | [![00000032.png](./00000032.png)](00000032.md) |
|  **33. Fill in the general settings** | **34. Go to the next page** | **35. Add client authentication** | **36. Go to the next page, again** |
|  [![00000033.png](./00000033.png)](00000033.md) | [![00000034.png](./00000034.png)](00000034.md) | [![00000035.png](./00000035.png)](00000035.md) | [![00000036.png](./00000036.png)](00000036.md) |
|  **37. Fill in the client settings** | **38. Submit the client form** | **39. Go to the Credentials tab** | **40. Get the client's secret** |
|  [![00000037.png](./00000037.png)](00000037.md) | [![00000038.png](./00000038.png)](00000038.md) | [![00000039.png](./00000039.png)](00000039.md) | [![00000040.png](./00000040.png)](00000040.md) |
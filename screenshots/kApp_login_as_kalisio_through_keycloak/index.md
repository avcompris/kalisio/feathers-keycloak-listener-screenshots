

_[Back to the home page](../../README.md)_




# Project: feathers-keycloak-listener



## Test: kApp_login_as_kalisio_through_keycloak.js


### Description




 
> logs in the kApp with the newly created user

### Steps (8) — 27 sec
1. [Open the kApp](00000001.md)
2. [Dismiss the modal dialog](00000002.md)
3. [Choose "Login with Keycloak"](00000003.md)
4. [Fill in the login form](00000004.md)
5. [Actually log in](00000005.md)
6. [Dismiss the modal dialog](00000006.md)
7. [Open the sidebar](00000007.md)
8. [Log out](00000008.md)


### Screenshots


|  **1. Open the kApp** | **2. Dismiss the modal dialog** | **3. Choose "Login with Keycloak"** | **4. Fill in the login form** |
| :--: | :--: | :--: | :--: |
|  [![00000001.png](./00000001.png)](00000001.md) | [![00000002.png](./00000002.png)](00000002.md) | [![00000003.png](./00000003.png)](00000003.md) | [![00000004.png](./00000004.png)](00000004.md) |
|  **5. Actually log in** | **6. Dismiss the modal dialog** | **7. Open the sidebar** | **8. Log out** |
|  [![00000005.png](./00000005.png)](00000005.md) | [![00000006.png](./00000006.png)](00000006.md) | [![00000007.png](./00000007.png)](00000007.md) | [![00000008.png](./00000008.png)](00000008.md) |


_[Back to the home page](../../README.md)_




# Project: feathers-keycloak-listener



## Test: keycloak_delete_user_previously_created.js


### Description




 
> sets up new user in Keycloak for integration tests with the kApp

### Steps (21) — 50 sec
1. [Keycloak: Login page](00000001.md)
2. [Credentials](00000002.md)
3. [Submit the login form](00000003.md)
4. [Deploy the realm list](00000004.md)
5. [Select the "Kalisio" realm](00000005.md)
6. [Go to the users page](00000006.md)
7. [Go to the keycloak-event-gateway user](00000007.md)
8. [Go to the Attributes tab](00000008.md)
9. [Fill in value for attribute: accessToken](00000009.md)
10. [Save the attributes](00000010.md)
11. [Go to the users page](00000011.md)
12. [Search for the user previously created](00000012.md)
13. [Select the user](00000013.md)
14. [Deploy the action menu](00000014.md)
15. [Ask to delete the user](00000015.md)
16. [Confirm the user deletion](00000016.md)
17. [Go to the users page](00000017.md)
18. [Go to the keycloak-event-gateway user](00000018.md)
19. [Go to the Attributes tab](00000019.md)
20. [Fill in value for attribute: accessToken](00000020.md)
21. [Save the attributes](00000021.md)


### Screenshots


|  **1. Keycloak: Login page** | **2. Credentials** | **3. Submit the login form** | **4. Deploy the realm list** |
| :--: | :--: | :--: | :--: |
|  [![00000001.png](./00000001.png)](00000001.md) | [![00000002.png](./00000002.png)](00000002.md) | [![00000003.png](./00000003.png)](00000003.md) | [![00000004.png](./00000004.png)](00000004.md) |
|  **5. Select the "Kalisio" realm** | **6. Go to the users page** | **7. Go to the keycloak-event-gateway user** | **8. Go to the Attributes tab** |
|  [![00000005.png](./00000005.png)](00000005.md) | [![00000006.png](./00000006.png)](00000006.md) | [![00000007.png](./00000007.png)](00000007.md) | [![00000008.png](./00000008.png)](00000008.md) |
|  **9. Fill in value for attribute: accessToken** | **10. Save the attributes** | **11. Go to the users page** | **12. Search for the user previously created** |
|  [![00000009.png](./00000009.png)](00000009.md) | [![00000010.png](./00000010.png)](00000010.md) | [![00000011.png](./00000011.png)](00000011.md) | [![00000012.png](./00000012.png)](00000012.md) |
|  **13. Select the user** | **14. Deploy the action menu** | **15. Ask to delete the user** | **16. Confirm the user deletion** |
|  [![00000013.png](./00000013.png)](00000013.md) | [![00000014.png](./00000014.png)](00000014.md) | [![00000015.png](./00000015.png)](00000015.md) | [![00000016.png](./00000016.png)](00000016.md) |
|  **17. Go to the users page** | **18. Go to the keycloak-event-gateway user** | **19. Go to the Attributes tab** | **20. Fill in value for attribute: accessToken** |
|  [![00000017.png](./00000017.png)](00000017.md) | [![00000018.png](./00000018.png)](00000018.md) | [![00000019.png](./00000019.png)](00000019.md) | [![00000020.png](./00000020.png)](00000020.md) |
|  **21. Save the attributes** |
|  [![00000021.png](./00000021.png)](00000021.md) |
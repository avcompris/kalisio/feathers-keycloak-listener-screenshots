

_[Back to the home page](../../README.md)_




# Project: feathers-keycloak-listener



## Test: kApp_login_with_new_user.js


### Description




 
> logs in the kApp with the newly created user

### Steps (6) — 17 sec
1. [Open the kApp](00000001.md)
2. [Dismiss the modal dialog](00000002.md)
3. [Fill in the login form](00000003.md)
4. [Actually log in](00000004.md)
5. [Open the sidebar](00000005.md)
6. [Log out](00000006.md)


### Screenshots


|  **1. Open the kApp** | **2. Dismiss the modal dialog** | **3. Fill in the login form** | **4. Actually log in** |
| :--: | :--: | :--: | :--: |
|  [![00000001.png](./00000001.png)](00000001.md) | [![00000002.png](./00000002.png)](00000002.md) | [![00000003.png](./00000003.png)](00000003.md) | [![00000004.png](./00000004.png)](00000004.md) |
|  **5. Open the sidebar** | **6. Log out** |
|  [![00000005.png](./00000005.png)](00000005.md) | [![00000006.png](./00000006.png)](00000006.md) |
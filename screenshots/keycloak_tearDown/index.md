

_[Back to the home page](../../README.md)_




# Project: feathers-keycloak-listener



## Test: keycloak_tearDown.js


### Description




 
> tears down the Keycloak configuration so we can run the tests again

### Steps (9) — 26 sec
1. [Keycloak: Login page](00000001.md)
2. [Credentials](00000002.md)
3. [Submit the login form](00000003.md)
4. [Deploy the realm list](00000004.md)
5. [Select the "Kalisio" realm](00000005.md)
6. [Go to the realm settings](00000006.md)
7. [Deploy the action menu](00000007.md)
8. [Ask to delete the realm](00000008.md)
9. [Confirm the realm deletion](00000009.md)


### Screenshots


|  **1. Keycloak: Login page** | **2. Credentials** | **3. Submit the login form** | **4. Deploy the realm list** |
| :--: | :--: | :--: | :--: |
|  [![00000001.png](./00000001.png)](00000001.md) | [![00000002.png](./00000002.png)](00000002.md) | [![00000003.png](./00000003.png)](00000003.md) | [![00000004.png](./00000004.png)](00000004.md) |
|  **5. Select the "Kalisio" realm** | **6. Go to the realm settings** | **7. Deploy the action menu** | **8. Ask to delete the realm** |
|  [![00000005.png](./00000005.png)](00000005.md) | [![00000006.png](./00000006.png)](00000006.md) | [![00000007.png](./00000007.png)](00000007.md) | [![00000008.png](./00000008.png)](00000008.md) |
|  **9. Confirm the realm deletion** |
|  [![00000009.png](./00000009.png)](00000009.md) |
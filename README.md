

# feathers-keycloak-listener-screenshots

These are some
screenshots captured by the integration tests from 
the [feathers-keycloak-listener](https://github.com/kalisio/feathers-keycloak-listener/) project.

 
The tests are listed in the order they are run by
the continuous integration.

| Test | Description | Elapsed Time | Screenshots |
| :-- | :-- | :--: | :--: |
| [`keycloak_setUp.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/keycloak_setUp.js) | sets up Keycloak for integration tests with the kApp | 55 sec | [40](screenshots/keycloak_setUp/index.md) |	
| [`kApp_login_as_kalisio.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/kApp_login_as_kalisio.js) | logs in the kApp | 17 sec | [6](screenshots/kApp_login_as_kalisio/index.md) |	
| [`kApp_login_as_kalisio_through_keycloak.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/kApp_login_as_kalisio_through_keycloak.js) | logs in the kApp with the newly created user | 27 sec | [8](screenshots/kApp_login_as_kalisio_through_keycloak/index.md) |	
| [`keycloak_create_new_user.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/keycloak_create_new_user.js) | sets up new user in Keycloak for integration tests with the kApp | 38 sec | [19](screenshots/keycloak_create_new_user/index.md) |	
| [`kApp_login_with_new_user_through_keycloak.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/kApp_login_with_new_user_through_keycloak.js) | logs in the kApp with the newly created user | 27 sec | [8](screenshots/kApp_login_with_new_user_through_keycloak/index.md) |	
| [`kApp_login_with_new_user.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/kApp_login_with_new_user.js) | logs in the kApp with the newly created user | 17 sec | [6](screenshots/kApp_login_with_new_user/index.md) |	
| [`keycloak_delete_user_previously_created.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/keycloak_delete_user_previously_created.js) | sets up new user in Keycloak for integration tests with the kApp | 50 sec | [21](screenshots/keycloak_delete_user_previously_created/index.md) |	
| [`keycloak_tearDown.js`](https://github.com/kalisio/feathers-keycloak-listener/blob/ab5e742ca01cb521df211b8fb6d523b40c63588b/test/keycloak_tearDown.js) | tears down the Keycloak configuration so we can run the tests again | 26 sec | [9](screenshots/keycloak_tearDown/index.md) |	
